using NUnit.Framework;
using StreamSplitter;
using System.IO;
using System.Linq;

namespace Tests
{
    public class TextSplitterTests
    {
        private static readonly string TEST_PATH = Path.Combine(TestContext.CurrentContext.TestDirectory, nameof(TextSplitterTests));

        [SetUp]
        public void Setup()
        {
            Directory.CreateDirectory(TEST_PATH);
        }

        [TearDown]
        public void TearDown()
        {
            Directory.Delete(TEST_PATH, true);
        }

        [Test]
        public void SplitStreamIntoMultipleFiles_AllEntriesArePresentAndInTheRightOrder()
        {
            using (var memoryStream = new MemoryStream())
            using (var streamWriter = new StreamWriter(memoryStream))
            {
                // SETUP
                var lines = Enumerable
                    .Range(0, 2000)
                    .Select(i => $"Lorem ipsum dolor sit amet, consetetur {i} sadipscing elitr, sed diam nonumy")
                    .ToList();
                foreach (var line in lines)
                {
                    streamWriter.WriteLine(line);
                }
                streamWriter.Flush();
                memoryStream.Seek(0, SeekOrigin.Begin);

                // TEST
                const string outputFormat = "test-{0:D5}.txt";
                var splitter = new TextStreamSplitter(outputFormat, 1024);
                var outputDirectory = new DirectoryInfo(TEST_PATH);
                splitter.Split(memoryStream, outputDirectory);

                // ASSERT
                var firstOutputFile = Path.Combine(TEST_PATH, string.Format(outputFormat, 1));
                Assert.IsTrue(File.Exists(firstOutputFile), "First output file does not exist.");
                var createdFiles = outputDirectory.EnumerateFiles().ToList();
                Assert.Greater(createdFiles.Count(), 1, "Expected more than one output file.");
                var readLines = createdFiles
                    .Select(file => file.FullName)
                    .OrderBy(f => f)
                    .SelectMany(File.ReadAllLines)
                    .ToList();
                Assert.AreEqual(lines.Count, readLines.Count, "Expected the same amount of input and output lines.");
                for (int i = 0; i < lines.Count; i++)
                {
                    Assert.AreEqual(lines[i], readLines[i], "Expected the same line at the same position.");
                }
            }
        }
    }
}