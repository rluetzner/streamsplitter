﻿using System.Collections.Generic;
using System.IO;

namespace StreamSplitter
{
    public class TextStreamSplitter
    {
        private readonly string _outputFormat;
        private readonly long _splitSizeInBytes;               

        public TextStreamSplitter(string outputPattern, long splitSizeInBytes)
        {
            _outputFormat = outputPattern;
            _splitSizeInBytes = splitSizeInBytes;
        }

        public void Split(Stream inputStream, DirectoryInfo outputDirectory)
        {
            Directory.CreateDirectory(outputDirectory.FullName);
            var inputLines = readLines(inputStream);
            using (var output = new StreamSwitcher(outputDirectory, _outputFormat, _splitSizeInBytes))
            {
                foreach (var line in inputLines)
                {
                    output.WriteLine(line);
                }
            }
        }

        private static IEnumerable<string> readLines(Stream inputStream)
        {
            var reader = new StreamReader(inputStream);
            while (!reader.EndOfStream)
            {
                yield return reader.ReadLine();
            }
        }       
    }
}
