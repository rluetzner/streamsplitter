﻿using System;
using System.IO;

namespace StreamSplitter
{
    internal class StreamSwitcher : IDisposable
    {
        private readonly string _outputFormat;
        private readonly DirectoryInfo _outputDirectory;
        private long _splitSizeInBytes;

        private StreamWriter _outputWriter;
        private int _currentIndex = 1;
        
        public StreamSwitcher(DirectoryInfo outputDirectory, string outputFormat, long splitSizeInBytes)
        {
            _outputDirectory = outputDirectory;
            _outputFormat = outputFormat;
            _splitSizeInBytes = splitSizeInBytes;
            _outputWriter = getNextOutputStream(_outputDirectory);
        }

        private StreamWriter getNextOutputStream(DirectoryInfo outputDirectory)
        {
            while (doesFileExist(outputDirectory, _currentIndex))
            {
                _currentIndex++;
            }
            var filePath = getFilePath(outputDirectory, _currentIndex);
            var fs = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write, FileShare.Read);
            return new StreamWriter(fs);
        }

        private bool doesFileExist(DirectoryInfo outputDirectory, int index)
        {
            var filePath = getFilePath(outputDirectory, index);
            return File.Exists(filePath);
        }

        private string getFilePath(DirectoryInfo outputDirectory, int index)
        {
            return Path.Combine(outputDirectory.FullName, string.Format(_outputFormat, index));
        }

        public void WriteLine(string line)
        {
            if (_outputWriter.BaseStream.Length >= _splitSizeInBytes)
            {
                _outputWriter?.Flush();
                _outputWriter?.Dispose();
                _currentIndex++;
                _outputWriter = getNextOutputStream(_outputDirectory);
            }
            _outputWriter.WriteLine(line);
            _outputWriter.Flush();
        }

        public void Dispose()
        {
            _outputWriter?.Flush();
            _outputWriter?.Dispose();
        }
    }
}
