# StreamSplitter

StreamSplitter is a C# library to split Streams containing text into multiple output files.

## Installation

NuGet package does not yet exist, but is planned for the future.

## Usage

```csharp
// Define an output format for your files.
string outputFormat = "outputfile-{0:D5}.txt";

// Define the split size in bytes.
long splitSizeInBytes = 1024;

// Define the output directory where files will be written to.
var outputDirectory = new DirectoryInfo(@"C:\TextStreamSplitterTest");

// Initialize the TextStreamSplitter.
TextStreamSplitter splitter = new TextStreamSplitter(outputFormat, splitSizeInBytes);

// Split!
splitter.Split(memoryStream, outputDirectory);
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)